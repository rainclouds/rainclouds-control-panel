const util = require('util');
const fs = require('fs');

const parseBool = (value, default_value) => {
    if(value !== 'true' && value !== 'false') return default_value;
    else if (value === 'true') return true;
    else if (value === 'false') return false;
}

module.exports = {
    libraries: [
        ["/public/lib/axios", "/node_modules/axios/dist"],
        ["/public/lib/mantine", "/node_modules/@mantine/core"],
        ["/public/lib/jquery", "/node_modules/jquery/dist"]
    ],

    // SERVICE CONFIGURATION
    service_domain: process.env.SERVICE_DOMAIN || "127.0.0.1",
    service_root: process.env.SERVICE_ROOT || "/service",
    session_default_ttl: process.env.SESSION_DEFAULT_TTL || 3600,

    // HTTP CONFIGURATION
    serve_http: parseBool(process.env.SERVE_HTTP, true),
    service_http_port: process.env.SERVICE_HTTP_PORT || 3080,
    service_http_address: process.env.SERVICE_HTTP_ADDRESS || "127.0.0.1",

    // HTTPS CONFIGURATION
    serve_https: parseBool(process.env.SERVE_HTTPS, false),
    service_https_port: process.env.SERVICE_HTTPS_PORT || 3443,
    service_https_address: process.env.SERVICE_HTTPS_ADDRESS || "127.0.0.1",
    service_certificate: {
        key: process.env.SERVICE_KEY ? fs.readFileSync(process.env.SERVICE_KEY, 'utf8') : null,
        cert: process.env.SERVICE_CERT ? fs.readFileSync(process.env.SERVICE_CERT, 'utf8') : null,
        ca: process.env.SERVICE_CA ? fs.readFileSync(process.env.SERVICE_CA, 'utf8') : null
    }
}