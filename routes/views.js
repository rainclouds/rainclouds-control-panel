// Gateways are rendered forms that communicate directly with the backend-api
module.exports = (app, rootPath) => {
    const localRoot = rootPath + '/';

    // Index Binding
    app.get(localRoot, (req, res) => {
        res.render("index", {"name": "Dylan"});
    });

    // Verify - Avoids displaying user input error, redirects on successful run
    // app.get(localRoot + '/session', async (req, res, next) => {
    //     try {
    //         const sessionKey = req.query.s || null;
    //         return res.render("forms/sessionFetcher", {"sessionKey": sessionKey});
    //     } catch (err) { return next(err); }
    // });
}